using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPS_Controller : MonoBehaviour
{
    public float WalkingSpeed = 7.5f;
    public float RunningSpeed = 11.5f;
    public float JumpSpeed = 20.0f;
    public Camera PlayerCamera;
    public float LookSpeed = 45.0f;
    public float LookXLimit = 45.0f;

    private Vector3 _moveDirection = Vector3.zero;
    private float _rotationX = 0;
    public bool CanMove = true;

    public GameObject GunObject;
    //public GameObject Body;
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    void Update()
    {
        Vector3 forward = transform.TransformDirection(Vector3.forward);
        Vector3 right = transform.TransformDirection(Vector3.right);

        bool isRunning = Input.GetKey(KeyCode.LeftShift);
        float currSpeedX = CanMove ? (isRunning ? RunningSpeed : WalkingSpeed) * Input.GetAxis("Vertical") : 0;
        float currSpeedY = CanMove ? (isRunning ? RunningSpeed : WalkingSpeed) * Input.GetAxis("Horizontal") : 0;
        float movementDirectionY = _moveDirection.y;
        _moveDirection = (forward * currSpeedX) + (right * currSpeedY);

        if (Input.GetButton("Jump") && CanMove)
        {
            _moveDirection.y = JumpSpeed;
        }
        /*else
        {
            _moveDirection.y = movementDirectionY;
        }*/
        
        transform.position += _moveDirection * Time.deltaTime;

        if (CanMove)
        {
            _rotationX += -Input.GetAxis("Mouse Y") * LookSpeed;
            _rotationX = Mathf.Clamp(_rotationX, -LookXLimit, LookXLimit);
            PlayerCamera.transform.localRotation = Quaternion.Euler(_rotationX, 0, 0);
            GunObject.transform.localRotation = Quaternion.Euler(_rotationX, 0, 0);
            transform.rotation *= Quaternion.Euler(0, Input.GetAxis("Mouse X") * LookSpeed, 0);
        }
    }
}
