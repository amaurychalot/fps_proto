using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class Shooting : MonoBehaviour
{
    public Camera PlayerCamera;
    public GameObject BulletSpawnPos;
    public ParticleSystem Explosion;
    public Text BulletCounter;
    public Animation Animation;
    public AnimationClip ShootAnimation;
    public AnimationClip ReloadAnimation;

    public int Ammo;

    public bool CanShoot = true;
    public bool Aiming;
    public bool OnAnimation;
    
    public GameObject BulletPrefab;
    // Start is called before the first frame update
    void Start()
    {
        Ammo = 3;
        Aiming = false;
        OnAnimation = false;
        BulletCounter.text = Ammo.ToString();
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            if (!Aiming && !OnAnimation)
            {
                OnAnimation = true;
                transform.DOLocalMoveX(-0.34f, 0.5f, false).OnComplete(() =>
                {
                    Aiming = true;
                    OnAnimation = false;
                });
            }
            else if (Aiming && !OnAnimation)
            {
                OnAnimation = true;
                transform.DOLocalMoveX(0f, 0.5f, false).OnComplete(() =>
                {
                    Aiming = false;
                    OnAnimation = false;
                });
            }
        }
    
        if (Input.GetMouseButtonDown(0))
        {
            if (Ammo > 0 && CanShoot)
            {
                GameObject bulletObject = Instantiate(BulletPrefab);
                bulletObject.transform.position = BulletSpawnPos.transform.position;
                bulletObject.transform.forward = PlayerCamera.transform.forward;
                var exp = Instantiate(Explosion);
                exp.transform.forward = PlayerCamera.transform.forward;
                exp.transform.position = BulletSpawnPos.transform.position;
                Ammo--;
                CanShoot = false;
                Animation.Play("ShootAnimation");
                Invoke("PlayerCanShoot", 0.5f);
                BulletCounter.text = Ammo.ToString();
            }
        }
        

        if (Input.GetKey(KeyCode.R))
        {
            Animation.AddClip(ReloadAnimation, "ReloadAnimation");
            Animation.Play("ReloadAnimation");
            Invoke("ReloadGun", 0.8f);
        }
    }

    void PlayerCanShoot()
    {
        CanShoot = true;
    }
    void ReloadGun()
    {
        Ammo = 3;
        BulletCounter.text = Ammo.ToString();
    }
}
